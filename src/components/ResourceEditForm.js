import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Card, CardSection, Input, Header} from './common';
import {Actions} from 'react-native-router-flux';
import {
	quantityChanged,
	markAsMissing,
	confirmQuantity,
	updateQuantity
} from '../actions';

class ResourceEditForm extends Component {
	state = {quantity: 0};
	onInputChange(text) {
		this.setState({quantity: Number(text)});
	}

	onConfirmClick() {
		this.props.confirmQuantity({resourceId: this.props.resource.ResourceId});
		Actions.pop();
		Actions.pop();
	}

	onUpdateClick() {
		this.props.updateQuantity({
			resourceId: this.props.resource.ResourceId,
			quantity: this.state.quantity
		});
		Actions.pop();
		Actions.pop();
	}

	onMarkClick() {
		this.props.markAsMissing({resourceId: this.props.resource.ResourceId});
		Actions.pop();
		Actions.pop();
	}

	renderConfirm() {
		if (this.props.resource.Verified) {
			return (
				<Button
					disabled
					style={{backgroundColor: '#DCDCDC'}}
					onPress={this.onConfirmClick.bind(this)}
				>
					Confirm Quantity
				</Button>
			);
		}
		return (
			<Button onPress={this.onConfirmClick.bind(this)}>Confirm Quantity</Button>
		);
	}

	renderUpdate() {
		if (this.props.resource.Verified) {
			return (
				<Button
					disabled
					style={{backgroundColor: '#DCDCDC'}}
					onPress={this.onUpdateClick.bind(this)}
				>
					Update Quantity
				</Button>
			);
		}
		return (
			<Button onPress={this.onUpdateClick.bind(this)}>Update Quantity</Button>
		);
	}

	renderMarkMissing() {
		if (this.props.resource.Verified) {
			return (
				<Button
					disabled
					style={{backgroundColor: '#DCDCDC'}}
					onPress={this.onMarkClick.bind(this)}
				>
					Mark as Missing
				</Button>
			);
		}
		return (
			<Button onPress={this.onMarkClick.bind(this)}>Mark as Missing</Button>
		);
	}

	render() {
		return (
			<Card>
				<Header headerText={this.props.resource.Name} />
				<CardSection>
					<Input
						label="Quantity"
						placeholder="10"
						value={String(this.props.resource.Quantity)}
						onChangeText={this.onInputChange.bind(this)}
					/>
				</CardSection>
				<CardSection>{this.renderConfirm()}</CardSection>
				<CardSection>{this.renderUpdate()}</CardSection>
				<CardSection>{this.renderMarkMissing()}</CardSection>
			</Card>
		);
	}
}

export default connect(null, {
	quantityChanged,
	confirmQuantity,
	markAsMissing,
	updateQuantity
})(ResourceEditForm);
